package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    @Test
    void gildedRoseMustStoreTheItemsPassedToIt() {
        Item[] items = new Item[]{new Item("foo", 0, 0)};
        GildedRose app = new GildedRose(items);

        assertEquals("foo", getNameOfFirstItem(app));
    }

    @Test
    void shouldDecreaseBothSellInAndQualityAtEndOfEachDay() {
        Item[] items = new Item[]{new Item("foo", 2, 2)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(1, getSellInOfFirstItem(app));
        assertEquals(1, getQualityOfFirstItem(app));
    }

    @Test
    void shouldDecreaseQualityTwiceWhenSellInIsPassed() {
        Item[] items = new Item[]{new Item("foo", 0, 5)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(3, getQualityOfFirstItem(app));
    }

    @Test
    void shouldNotDecreaseQualityToNegative() {
        Item[] items = new Item[]{new Item("foo", 0, 0)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(-1, getSellInOfFirstItem(app));
        assertEquals(0, getQualityOfFirstItem(app));
    }

    @Test
    void shouldIncreaseQualityOfAgedBrieAtEndOfEachDay() {
        Item[] items = new Item[]{new Item("Aged Brie", 1, 0)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(0, getSellInOfFirstItem(app));
        assertEquals(1, getQualityOfFirstItem(app));
    }

    @Test
    void shouldIncreaseQualityOfAgedBrieTwiceWhenSellInIsPassed() {
        Item[] items = new Item[]{new Item("Aged Brie", 0, 0)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(-1, getSellInOfFirstItem(app));
        assertEquals(2, getQualityOfFirstItem(app));
    }

    @Test
    void shouldNotIncreaseQualityOverFifty() {
        Item[] items = new Item[]{new Item("Aged Brie", 0, 50)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(50, getQualityOfFirstItem(app));
    }

    @Test
    void shouldNotAlterSulfurasAtEndOfEachDay() {
        Item[] items = new Item[]{new Item("Sulfuras, Hand of Ragnaros", 0, 80)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(0, getSellInOfFirstItem(app));
        assertEquals(80, getQualityOfFirstItem(app));
    }

    @Test
    void shouldIncreaseQualityOfBackstagePassesAtEndOfEachDay() {
        Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert", 11, 0)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(1, getQualityOfFirstItem(app));
    }

    @Test
    void shouldIncreaseQualityOfBackstagePassesTwiceWhenLessThanElevenSellInDays() {
        Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert", 10, 0)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(2, getQualityOfFirstItem(app));
    }

    @Test
    void shouldIncreaseQualityOfBackstagePassesThriceWhenLessThanSixSellInDays() {
        Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert", 5, 0)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(3, getQualityOfFirstItem(app));
    }

    @Test
    void shouldZeroQualityOfBackstagePassesWhenConcertIsOver() {
        Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert", 0, 10)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(-1, getSellInOfFirstItem(app));
        assertEquals(0, getQualityOfFirstItem(app));
    }

    @Test
    void shouldDecreaseQualityOfConjuredTwiceAsFastAsNormalItems() {
        Item[] items = new Item[]{new Item("Conjured Mana Cake", 5, 5)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(4, getSellInOfFirstItem(app));
        assertEquals(3, getQualityOfFirstItem(app));
    }

    @Test
    void shouldDecreaseQualityOfConjuredTwiceWhenSellInIsPassed() {
        Item[] items = new Item[]{new Item("Conjured Mana Cake", 0, 5)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(1, getQualityOfFirstItem(app));
    }

    private String getNameOfFirstItem(GildedRose app) {
        return app.getItems()[0].name;
    }

    private int getQualityOfFirstItem(GildedRose app) {
        return app.getItems()[0].quality;
    }

    private int getSellInOfFirstItem(GildedRose app) {
        return app.getItems()[0].sellIn;
    }

}
