package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    void shouldCreateItemWithValuesPassedAsArguments() {
        Item item = new Item("test", 2, 5);

        assertEquals(item.name, "test");
        assertEquals(item.sellIn, 2);
        assertEquals(item.quality, 5);
    }
}
