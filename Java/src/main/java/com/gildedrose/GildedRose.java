package com.gildedrose;

class GildedRose {
    private final Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public Item[] getItems() {
        return items;
    }

    public void updateQuality() {
        for (Item item : items) {
            if (isNotSulfuras(item)) {
                switch (item.name) {
                    case "Aged Brie":
                        if (isSellByDatePassed(item)) incrementQualityBy(item, 2);
                        else incrementQualityBy(item, 1);
                        break;
                    case "Backstage passes to a TAFKAL80ETC concert":
                        if (isSellByDatePassed(item)) item.quality = 0;
                        else incrementBackstagePassesQualityBasedOnSellIn(item);
                        break;
                    case "Conjured Mana Cake":
                        if (isSellByDatePassed(item)) decrementQualityBy(item, 4);
                        else decrementQualityBy(item, 2);
                        break;
                    default:
                        if (isSellByDatePassed(item)) decrementQualityBy(item, 2);
                        else decrementQualityBy(item, 1);
                }
                item.sellIn -= 1;
            }

        }
    }


    private boolean isNotSulfuras(Item item) {
        return !item.name.equals("Sulfuras, Hand of Ragnaros");
    }

    private boolean isSellByDatePassed(Item item) {
        return item.sellIn <= 0;
    }

    private void incrementQualityBy(Item item, int value) {
        int MAX_QUALITY = 50;

        if (item.quality + value < MAX_QUALITY) {
            item.quality += value;
        } else item.quality = MAX_QUALITY;
    }


    private void decrementQualityBy(Item item, int value) {
        int MIN_QUALITY = 0;

        if (item.quality - value > MIN_QUALITY) {
            item.quality -= value;
        } else item.quality = MIN_QUALITY;
    }

    private void incrementBackstagePassesQualityBasedOnSellIn(Item item) {
        if (item.sellIn <= 5) incrementQualityBy(item, 3);
        else if (item.sellIn <= 10) incrementQualityBy(item, 2);
        else incrementQualityBy(item, 1);
    }

}
